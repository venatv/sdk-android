package tv.vena.mraid;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import tv.vena.mraidsdk.MraidSdk;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button showAdButton = (Button) findViewById(R.id.showAdButton);
        showAdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MraidSdk.showAd(MainActivity.this);
            }
        });

        MraidSdk.setAdListener(new MraidSdk.AdCompleteListener() {
            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete: Ad is complete");
                Toast.makeText(MainActivity.this, "Success!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(String msg) {
                Log.d(TAG, "onError: " + msg);
                Toast.makeText(MainActivity.this, "Failure! " + msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        MraidSdk.releaseAdListener();
    }
}