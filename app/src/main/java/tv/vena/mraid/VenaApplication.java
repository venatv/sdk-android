package tv.vena.mraid;

import android.app.Application;

import tv.vena.mraidsdk.MraidSdk;

public class VenaApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        MraidSdk.init(this, 21);
    }
}
