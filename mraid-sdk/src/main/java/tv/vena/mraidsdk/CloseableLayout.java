package tv.vena.mraidsdk;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.StateListDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.ViewConfiguration;
import android.widget.FrameLayout;

class CloseableLayout extends FrameLayout {
    public interface OnCloseListener {
        void onClose();
    }

    @VisibleForTesting
    static final float CLOSE_BUTTON_SIZE_DP = 30.0f;
    static final float CLOSE_REGION_SIZE_DP = 50.0f;

    @VisibleForTesting
    static final float CLOSE_BUTTON_PADDING_DP = 8.0f;

    public enum ClosePosition {
        TOP_LEFT(Gravity.TOP | Gravity.LEFT),
        TOP_CENTER(Gravity.TOP | Gravity.CENTER_HORIZONTAL),
        TOP_RIGHT(Gravity.TOP | Gravity.RIGHT),
        CENTER(Gravity.CENTER),
        BOTTOM_LEFT(Gravity.BOTTOM | Gravity.LEFT),
        BOTTOM_CENTER(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL),
        BOTTOM_RIGHT(Gravity.BOTTOM | Gravity.RIGHT);

        private final int mGravity;

        ClosePosition(final int mGravity) {
            this.mGravity = mGravity;
        }

        int getGravity() {
            return mGravity;
        }
    }

    private final int mTouchSlop;

    @Nullable
    private OnCloseListener mOnCloseListener;

    @NonNull
    private final StateListDrawable mCloseDrawable;
    @NonNull
    private ClosePosition mClosePosition;
    private final int mCloseRegionSize;  // Size of the touchable close region.
    private final int mCloseButtonSize;  // Size of the drawn close button.
    private final int mCloseButtonPadding;


    private boolean mCloseBoundChanged;

    private final Rect mClosableLayoutRect = new Rect();
    private final Rect mCloseRegionBounds = new Rect();
    private final Rect mCloseButtonBounds = new Rect();
    private final Rect mInsetCloseRegionBounds = new Rect();

    @Nullable
    private UnsetPressedState mUnsetPressedState;

    public CloseableLayout(@NonNull Context context) {
        super(context);

        mCloseDrawable = new StateListDrawable();
        mClosePosition = ClosePosition.TOP_RIGHT;

        mCloseDrawable.addState(SELECTED_STATE_SET,
                Drawables.INTERSTITIAL_CLOSE_BUTTON_PRESSED.createDrawable(context));
        mCloseDrawable.addState(EMPTY_STATE_SET,
                Drawables.INTERSTITIAL_CLOSE_BUTTON_NORMAL.createDrawable(context));

        mCloseDrawable.setState(EMPTY_STATE_SET);
        mCloseDrawable.setCallback(this);

        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();

        mCloseRegionSize = Dips.asIntPixels(CLOSE_REGION_SIZE_DP, context);
        mCloseButtonSize = Dips.asIntPixels(CLOSE_BUTTON_SIZE_DP, context);
        mCloseButtonPadding = Dips.asIntPixels(CLOSE_BUTTON_PADDING_DP, context);

        setWillNotDraw(false);
    }

    public void setOnCloseListener(@Nullable OnCloseListener onCloseListener) {
        mOnCloseListener = onCloseListener;
    }

    @Override
    protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight) {
        super.onSizeChanged(width, height, oldWidth, oldHeight);
        mCloseBoundChanged = true;
    }

    @Override
    public void draw(@NonNull final Canvas canvas) {
        super.draw(canvas);

        if (mCloseBoundChanged) {
            mCloseBoundChanged = false;

            mClosableLayoutRect.set(0, 0, getWidth(), getHeight());
            applyCloseRegionBounds(mClosePosition, mClosableLayoutRect, mCloseRegionBounds);

            mInsetCloseRegionBounds.set(mCloseRegionBounds);
            mInsetCloseRegionBounds.inset(mCloseButtonPadding, mCloseButtonPadding);

            applyCloseButtonBounds(mClosePosition, mInsetCloseRegionBounds, mCloseButtonBounds);
            mCloseDrawable.setBounds(mCloseButtonBounds);
        }

        if (mCloseDrawable.isVisible()) {
            mCloseDrawable.draw(canvas);
        }
    }

    public void applyCloseRegionBounds(ClosePosition closePosition, Rect bounds, Rect closeBounds) {
        applyCloseBoundsWithSize(closePosition, mCloseRegionSize, bounds, closeBounds);
    }

    private void applyCloseButtonBounds(ClosePosition closePosition, Rect bounds, Rect outBounds) {
        applyCloseBoundsWithSize(closePosition, mCloseButtonSize, bounds, outBounds);
    }

    private void applyCloseBoundsWithSize(ClosePosition closePosition, final int size, Rect bounds, Rect outBounds) {
        Gravity.apply(closePosition.getGravity(), size, size, bounds, outBounds);
    }

    @Override
    public boolean onInterceptTouchEvent(@NonNull final MotionEvent event) {
        if (event.getAction() != MotionEvent.ACTION_DOWN) {
            return false;
        }

        final int x = (int) event.getX();
        final int y = (int) event.getY();
        return pointInCloseBounds(x, y, 0);
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        final int x = (int) event.getX();
        final int y = (int) event.getY();
        if (!pointInCloseBounds(x, y, mTouchSlop)) {
            setClosePressed(false);
            super.onTouchEvent(event);
            return false;
        }

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                setClosePressed(true);
                break;
            case MotionEvent.ACTION_CANCEL:
                setClosePressed(false);
                break;
            case MotionEvent.ACTION_UP:
                if (isClosePressed()) {
                    if (mUnsetPressedState == null) {
                        mUnsetPressedState = new UnsetPressedState();
                    }
                    postDelayed(mUnsetPressedState, ViewConfiguration.getPressedStateDuration());
                    performClose();
                }
                break;
        }
        return true;
    }

    private void setClosePressed(boolean pressed) {
        if (pressed == isClosePressed()) {
            return;
        }

        mCloseDrawable.setState(pressed ? SELECTED_STATE_SET : EMPTY_STATE_SET);
        invalidate(mCloseRegionBounds);
    }

    boolean isClosePressed() {
        return mCloseDrawable.getState() == SELECTED_STATE_SET;
    }

    boolean pointInCloseBounds(int x, int y, int slop) {
        return x >= mCloseRegionBounds.left - slop
                && y >= mCloseRegionBounds.top - slop
                && x < mCloseRegionBounds.right + slop
                && y < mCloseRegionBounds.bottom + slop;
    }

    private void performClose() {
        playSoundEffect(SoundEffectConstants.CLICK);
        if (mOnCloseListener != null) {
            mOnCloseListener.onClose();
        }
    }

    private final class UnsetPressedState implements Runnable {
        public void run() {
            setClosePressed(false);
        }
    }
}
