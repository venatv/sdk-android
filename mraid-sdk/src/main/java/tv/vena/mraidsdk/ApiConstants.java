package tv.vena.mraidsdk;

class ApiConstants {

    static final String BASE_URL = "http://dev.bidder.vena.tv";
    static final String PATH = "adrequest.php";
    static final String PARAM_PUBLISHER_ID = "publisher_id";
    static final String PARAM_APP_NAME = "app_name";
    static final String PARAM_APP_BUNDLE_ID = "app_bundle_id";
    static final String PARAM_USER_ID = "user_id";
    static final String PARAM_LATITUDE = "latitude";
    static final String PARAM_LONGITUDE = "longitude";
    static final String PARAM_IP = "ip";
    static final String PARAM_UA = "ua";
    static final String VENA_QUERY_ORDER_LINE = "order_line_id";
    static final String HEADER_PUBLISHER_ID = "publisher_id";
    static final String HEADER_USER_AGENT = "user_agent";
    static final String HEADER_IFA = "ifa";
    static final String HEADER_APP_NAME = "app_name";
    static final String HEADER_IP = "ip";
}
