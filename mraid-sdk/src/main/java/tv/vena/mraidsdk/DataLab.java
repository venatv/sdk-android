package tv.vena.mraidsdk;

class DataLab {

    private static DataLab sInstance;

    private static CreativeData sCreativeData;
    private ClientData mClientData;

    private DataLab() {}

    static DataLab getInstance() {
        if (sInstance == null) {
            sInstance = new DataLab();
        }
        return sInstance;
    }

    CreativeData getCreativeData() {
        return sCreativeData;
    }

    void setCreative(CreativeData creativeData) {
        if (creativeData == null) {
            sCreativeData = null;
        }

        sCreativeData = creativeData;
    }

    ClientData getClientData() {
        return mClientData;
    }

    void setClientData(ClientData clientData) {
        mClientData = clientData;
    }
}
