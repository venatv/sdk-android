package tv.vena.mraidsdk;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.TypedValue;

class Dips {

    private static float asFloatPixels(float dips, Context context) {
        final DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dips, displayMetrics);
    }

    static int asIntPixels(float dips, Context context) {
        return (int) (asFloatPixels(dips, context) + 0.5f);
    }

    static int getWidth(Context context) {
        final DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return(int) convertPixelsToDp(displayMetrics.widthPixels, context);
    }

    static int getHeight(Context context) {
        final DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return(int) convertPixelsToDp(displayMetrics.heightPixels, context);
    }

    private static float convertPixelsToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return px / ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }
}
