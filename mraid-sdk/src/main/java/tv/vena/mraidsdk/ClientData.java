package tv.vena.mraidsdk;

class ClientData {
    String mPublisherId;
    String mIp;
    String mUserAgent;
    String mAppName;
    String mIfa;
    String mLatitude;
    String mLongitude;
    String mPackage;

    @Override
    public String toString() {
        return "user agent: " + mUserAgent + "\n" +
                "publisher id: " + mPublisherId + "\n" +
                "ip: " + mIp + "\n" +
                "app name: " + mAppName + "\n" +
                "ifa: " + mIfa;
    }
}
