package tv.vena.mraidsdk;

import android.content.Context;

import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.config.Configuration;

class JobQueueManager {

    static JobManager sJobManager;

    static synchronized void initJobManager(Context context) {
        if (sJobManager == null) {
            configureJobManager(context);
        }
    }

    private static void configureJobManager(Context context) {
        Configuration.Builder builder = new Configuration.Builder(context)
                .minConsumerCount(1)
                .maxConsumerCount(3)
                .loadFactor(3)
                .consumerKeepAlive(120);

        sJobManager = new JobManager(builder.build());
    }
}
