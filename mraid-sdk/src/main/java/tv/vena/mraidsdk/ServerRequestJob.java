package tv.vena.mraidsdk;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;

class ServerRequestJob extends Job {

    private static final String TAG = ServerRequestJob.class.getSimpleName();

    private String mUrl;

    ServerRequestJob(String url) {
        super(new Params(1).requireNetwork());
        mUrl = url;
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        NetworkManager.requestServer(mUrl);
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {

    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        Log.v(TAG, "shouldReRunOnThrowable: retry task");
        return RetryConstraint.createExponentialBackoff(runCount, 20);
    }
}
