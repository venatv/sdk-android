package tv.vena.mraidsdk;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

class Bridge {

    private static final String TAG = Bridge.class.getSimpleName();

    private AdWebView mAdWebView;
    private BridgeListener mBridgeListener;

    private boolean mIsNotFirstLoading;

    interface BridgeListener {

        void onPageLoaded();

        void shouldOverrideUrlLoading(String url);
    }

    void setBridgeListener (BridgeListener listener) {
        mBridgeListener = listener;
    }

    void attachView(final AdWebView webView) {
        mAdWebView = webView;
        mAdWebView.getSettings().setJavaScriptEnabled(true);
        mAdWebView.enableJavascriptCaching();
        mAdWebView.setScrollContainer(false);
        mAdWebView.setVerticalScrollBarEnabled(false);
        mAdWebView.setHorizontalScrollBarEnabled(false);
        mAdWebView.setBackgroundColor(Color.BLACK);
        mAdWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Log.e(TAG, "Error: " + description);
                if (MraidSdk.sListener != null) {
                    MraidSdk.sListener.onError("Error" + description);
                }
                super.onReceivedError(view, errorCode, description, failingUrl);
            }

            @Override
            public boolean shouldOverrideUrlLoading(@NonNull WebView view, @NonNull String url) {
                handleShouldOverrideUrlLoading(url);
                return true;
            }

            @Override
            public void onPageFinished(@NonNull WebView view, @NonNull String url) {
                if (url.equals("about:blank") && mIsNotFirstLoading) return;
                handlePageFinished();
                mIsNotFirstLoading = true;
            }
        });
    }


    private void handlePageFinished() {
        if (mBridgeListener != null) {
            mBridgeListener.onPageLoaded();
        }
    }

    private void handleShouldOverrideUrlLoading(String url) {
        mBridgeListener.shouldOverrideUrlLoading(url);
    }

    void detach() {
        mAdWebView = null;
    }

    void setContentHtml(String htmlData) {
        if (mAdWebView == null) {
            Log.d(TAG, "MRAID bridge called setContentHtml before WebView was attached");
            return;
        }

        mAdWebView.loadDataWithBaseURL("",
                htmlData, "text/html", "UTF-8", null);
    }

    private void injectJavaScript(String javascript) {
        if (mAdWebView == null) {
            Log.d(TAG, "Attempted to inject Javascript into MRAID WebView while was not "
                    + "attached:\n\t" + javascript);
            return;
        }

        Log.v(TAG, "Injecting Javascript into MRAID WebView:\n\t" + javascript);
        mAdWebView.loadUrl("javascript:" + javascript);
    }

    void notifyReady() {
        injectJavaScript("mraidbridge.notifyReadyEvent();");
    }

    void fireChangeEvent(ViewState state, boolean isViewable) {
        injectJavaScript("mraidbridge.fireChangeEvent("
                + "{state: '"
                + state.toJavascriptString()
                + "',"
                + " viewable: '"
                + isViewable
                + "'"
                + "})");
    }

    void nativeCallComplete(MraidJavascriptCommand command) {
        injectJavaScript("mraidbridge.nativeCallComplete('"
                + command.toJavascriptString()
                + "');");
        mAdWebView.onResume();
    }

    private void setCurrentPosition(int x, int y, int width, int height) {
        injectJavaScript("mraidbridge.setCurrentPosition("
                + x
                + ", "
                + y
                + " , "
                + width
                + " , "
                + height
                + ")");
    }

    private void setDefaultPosition(int x, int y, int width, int height) {
        injectJavaScript("mraidbridge.setDefaultPosition("
                + x
                + ", "
                + y
                + " , "
                + width
                + " , "
                + height
                + ")");
    }

    private void setScreenSize(int width, int height) {
        injectJavaScript("mraidbridge.setScreenSize("
                + width
                + " , "
                + height
                + ")");
    }

    private void setMaxSize(int width, int height) {
        injectJavaScript("mraidbridge.setMaxSize("
                + width
                + " , "
                + height
                + ")");
    }

    private void notifySizeChangeEvent(int width, int height) {
        injectJavaScript("mraidbridge.notifySizeChangeEvent("
                + width
                + " , "
                + height
                + ")");
    }

    void setScreenParams(int x, int y, int width, int height) {
        setCurrentPosition(x, y, width, height);
        setDefaultPosition(x, y, width, height);
        setScreenSize(width, height);
        setMaxSize(width, height);
        notifySizeChangeEvent(width, height);
    }
}
