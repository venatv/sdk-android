package tv.vena.mraidsdk;

import android.app.Activity;
import android.content.Context;

public class MraidSdk {

    private static final String TAG = MraidSdk.class.getSimpleName();

    private static ClientData sClientData;
    private static DataLab sDataLab = DataLab.getInstance();

    static AdCompleteListener sListener;

    public interface AdCompleteListener {

        void onComplete();

        void onError(String msg);
    }

    public static void init(Context context, int publisherId) {
        createClientData(String.valueOf(publisherId));
        JobQueueManager.initJobManager(context);
    }

    private static void createClientData(String publisherId) {
        sClientData = new ClientData();
        sClientData.mPublisherId = publisherId;
        sDataLab.setClientData(sClientData);
    }

    public static void showAd(Context context) {
        if (sClientData == null) {
            Utils.showError((Activity)context, context.getString(R.string.error_init));
        } else {
            init(context.getApplicationContext(), Integer.valueOf(sClientData.mPublisherId));
            if (ConnectionChecker.networkIsAvailable(context)) {
                MraidActivity.start(context);
            } else {
               Utils.showError((Activity)context, context.getString(R.string.error_internet_connection));
            }
        }
    }

    public static void setAdListener(AdCompleteListener listener) {
        MraidSdk.sListener = listener;
    }

    public static void releaseAdListener() {
        if (sListener != null) {
            sListener = null;
        }
    }
}
