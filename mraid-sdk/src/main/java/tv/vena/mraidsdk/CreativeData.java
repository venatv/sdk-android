package tv.vena.mraidsdk;

import java.util.List;

class CreativeData {

    String mCreativeScript;
    List<String> mImpressions;
    List<String> mClicks;
    String mCloseEvent;
    String mFullScreenEvent;
}
