package tv.vena.mraidsdk;

import android.Manifest;
import android.app.ActionBar;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;

import java.io.IOException;
import java.util.List;

public final class MraidActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_LOAD_LOCATION = 11;

    private Controller mController;
    private ClientData mClientData;

    private interface Ifa {

        void requestCreative(String ifa);
    }

    interface DataLoaded {

        void onDataLoaded(String data);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            getWindow().setFlags(
                    WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                    WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        }

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            mController = new Controller(this);
            setContentView(mController.getAdContainer());

            fetchClientData();
        }
    }

    private void fetchClientData() {
        DataLab dataLab = DataLab.getInstance();
        mClientData = dataLab.getClientData();
        mClientData.mUserAgent = mController.getUserAgent();
        mClientData.mIp = Utils.getIPAddress(true);
        mClientData.mAppName = Utils.getApplicationName(this, getPackageManager());
        mClientData.mPackage = getApplicationContext().getPackageName();

        getLocation();
    }

    private void getIfa() {
        getIfa(new Ifa() {
            @Override
            public void requestCreative(String ifa) {
                mClientData.mIfa = ifa;
                        NetworkManager.createCreativeModel(MraidActivity.this, new DataLoaded() {
                            @Override
                            public void onDataLoaded(String data) {
                                mController.loadContent(data);
                            }
                        });
            }
        });
    }

    private void getLocation() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
            requestLocationPermissions();
        } else {
            setLocation();
            getIfa();
        }
    }

    private void requestLocationPermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION},
                MY_PERMISSIONS_REQUEST_LOAD_LOCATION);
    }

    private void setLocation() {
        Location location = getLastKnownLocation();

        if (location != null) {
            mClientData.mLatitude = String.valueOf(location.getLatitude());
            mClientData.mLongitude = String.valueOf(location.getLongitude());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOAD_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setLocation();
                }
                getIfa();
            }
        }
    }

    private Location getLastKnownLocation() {
        LocationManager locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }

            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                bestLocation = l;
            }
        }
        return bestLocation;
    }

    private void getIfa(final Ifa ifa) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    AdvertisingIdClient.Info adInfo = AdvertisingIdClient.getAdvertisingIdInfo(MraidActivity.this);
                    ifa.requestCreative(adInfo != null ? adInfo.getId() : null);
                } catch (IOException | GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException exception) {
                    exception.printStackTrace();
                }
            }
        });
    }

    static void start(@NonNull Context context) {
            try {
                Intent intent = new Intent(context, MraidActivity.class);
                context.startActivity(intent);
            } catch (ActivityNotFoundException exception) {
                Log.e("MraidInterstitial", "MraidActivity.class not found. Did you declare MraidActivity in your manifest?");
            }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mController != null) {
            mController.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onPostResume();

        if (mController != null) {
            mController.resume();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        mController.handleClose();
    }
}
