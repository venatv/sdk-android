package tv.vena.mraidsdk;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

class DataParser {

    private static final String TAG = DataParser.class.getSimpleName();
    private static final String XML_VAST_URI = "VASTAdTagURI";
    private static final String XML_TRACKING = "Tracking";
    private static final String XML_IMPRESSION = "Impression";
    private static final String XML_CLICK = "ClickTracking";
    private static final String XML_ATTRIBUTE_EVENT = "event";
    private static final String XML_ATTRIBUTE_VALUE_CLOSE = "close";
    private static final String XML_ATTRIBUTE_VALUE_FULLSCREEN = "fullscreen";

    static CreativeData parse(InputStream in) throws XmlPullParserException,
            IOException {
        CreativeData creativeData = null;
        ArrayList<String> impressions = null;
        ArrayList<String> clicks = null;

        XmlPullParser parser = Xml.newPullParser();
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        parser.setInput(in, null);

        int eventType = parser.getEventType();

        while (eventType != XmlPullParser.END_DOCUMENT) {
            String name;
            switch (eventType) {
                case XmlPullParser.START_DOCUMENT:
                    creativeData = new CreativeData();
                    break;
                case XmlPullParser.START_TAG:
                    name = parser.getName();
                    switch (name) {
                        case XML_VAST_URI:
                            moveToken(parser);
                            if (creativeData != null) {
                                creativeData.mCreativeScript = parser.getText();
                            }
                            break;
                        case XML_IMPRESSION:
                            if (impressions == null) {
                                impressions = new ArrayList<>();
                            }
                            moveToken(parser);
                            impressions.add(parser.getText());

                            break;
                        case XML_CLICK:
                            if (clicks == null) {
                                clicks = new ArrayList<>();
                            }
                            moveToken(parser);
                            clicks.add(parser.getText());
                            break;
                        case XML_TRACKING:
                            String event = parser.getAttributeValue(null, XML_ATTRIBUTE_EVENT);
                            if (event.equals(XML_ATTRIBUTE_VALUE_CLOSE)) {
                                moveToken(parser);
                                if (creativeData != null) {
                                    creativeData.mCloseEvent = parser.getText();
                                }
                            } else if (event.equals(XML_ATTRIBUTE_VALUE_FULLSCREEN)) {
                                if (creativeData != null) {
                                    moveToken(parser);
                                    creativeData.mFullScreenEvent = parser.getText();
                                }
                            }
                            break;
                    }
                    break;
                case XmlPullParser.END_TAG:
                    if (creativeData != null) {
                        creativeData.mImpressions = impressions;
                        creativeData.mClicks = clicks;
                    }
                    break;
            }
            eventType = parser.next();
        }

        return creativeData;
    }

    private static void moveToken(XmlPullParser parser) throws XmlPullParserException, IOException {
        int token = parser.nextToken();
        while(token != XmlPullParser.CDSECT){
            token = parser.nextToken();
        }
    }
}
