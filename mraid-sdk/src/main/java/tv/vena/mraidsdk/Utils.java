package tv.vena.mraidsdk;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URLDecoder;
import java.util.Collections;
import java.util.List;

class Utils {

    static final String MRAID_OPEN_URL = "mraid://open?url=";
    static final String CHARSET_UTF = "UTF-8";
    static final String MRAID_SCHEME = "mraid://";
    static final String REGEX = "[-!$%^&*()_+|~=`{}\\[\\]:\";'<>?,./]";

    static String createHtml(String scriptSource) {
        return "<!DOCTYPE html>\n" +
                "<head>" +
                "<meta name=\"viewport\" content=\"user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1.0\">" +
                "<body topmargin=\"0\" leftmargin=\"0\" rightmargin=\"0\" bottommargin=\"0\"></body>" +
                "</head>" +
                "<script type=\"text/javascript\" src=\"file:///android_asset/mraid.js\"></script>\n" +
//                "<script type=\"text/javascript\" src=\" " +
//                scriptSource +
                "<script>" +
                scriptSource +
                "</script>" +
                "\n</html>";
    }

    static String getCommandFromUrl(String url) {
        String [] splitted = url.split(MRAID_SCHEME);
        return splitted[1].split(REGEX)[0];
    }

    static String prepareUrl(String url) {
        try {
            url = url.replace(MRAID_OPEN_URL, "");
            url = URLDecoder.decode(url, CHARSET_UTF);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return url;
    }

    static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':')<0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%');
                                return delim < 0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    static String getApplicationName(Context context, PackageManager packageManager) {
        ApplicationInfo applicationInfo ;
        try {
            applicationInfo = packageManager.getApplicationInfo(context.getPackageName(), 0);
        } catch (final PackageManager.NameNotFoundException e) {
            applicationInfo = null;
        }
        return  (String) (applicationInfo != null ? packageManager.getApplicationLabel(applicationInfo ) : "(unknown)");
    }

    static void showError(Activity activity, final String msg) {
        if (MraidSdk.sListener != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    MraidSdk.sListener.onError(msg);
                }
            });
        }
    }
}
