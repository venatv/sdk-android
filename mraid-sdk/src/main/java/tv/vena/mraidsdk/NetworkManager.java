package tv.vena.mraidsdk;

import android.app.Activity;
import android.util.Log;
import android.webkit.URLUtil;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static tv.vena.mraidsdk.ApiConstants.BASE_URL;
import static tv.vena.mraidsdk.ApiConstants.PARAM_APP_BUNDLE_ID;
import static tv.vena.mraidsdk.ApiConstants.PARAM_APP_NAME;
import static tv.vena.mraidsdk.ApiConstants.PARAM_IP;
import static tv.vena.mraidsdk.ApiConstants.PARAM_LATITUDE;
import static tv.vena.mraidsdk.ApiConstants.PARAM_LONGITUDE;
import static tv.vena.mraidsdk.ApiConstants.PARAM_PUBLISHER_ID;
import static tv.vena.mraidsdk.ApiConstants.PARAM_UA;
import static tv.vena.mraidsdk.ApiConstants.PARAM_USER_ID;
import static tv.vena.mraidsdk.ApiConstants.PATH;
import static tv.vena.mraidsdk.MraidActivity.DataLoaded;

class NetworkManager {

    private static final String TAG = NetworkManager.class.getSimpleName();

    private static ClientData sClientData = DataLab.getInstance().getClientData();
    private static CreativeData sCreativeData;
    private static DataLab sDataLab = DataLab.getInstance();

    interface DataCallback {

        void onResponse (Response response);
    }

    static void createCreativeModel(final Activity activity, final DataLoaded dataLoaded) {
        DataCallback callback = new DataCallback() {
            @Override
            public void onResponse(Response response) {
                sCreativeData = getEntry(activity, response);
                sDataLab.setCreative(sCreativeData);
                if (sCreativeData == null) {
                    Utils.showError(activity, activity.getString(R.string.error_init));
                    return;
                }

                if (URLUtil.isValidUrl(sDataLab.getCreativeData().mCreativeScript)) {
                    loadHtmlContent(activity, sDataLab.getCreativeData().mCreativeScript, new DataCallback() {
//                        TODO: remove redudant calls

//                    NetworkManager.loadHtmlContent(activity, "http://platform.vena.tv/vm.php?tag=https%3A%2F%2Fbs.serving-sys.com%2FServing%3Fcn%3Ddisplay%26c%3D23%26pl%3DVAST%26pli%3D19460067%26PluID%3D0%26pos%3D3973%26ord%3D%5Btimestamp%5D%26cim%3D1", new DataCallback() {
//                    NetworkManager.loadHtmlContent(activity, "http://cdn-tags.brainient.com/1716/6b9e50ae-9a9e-4df0-95bf-97d598a43374/html5/embed.js?proto=http", new DataCallback() {
//                    NetworkManager.loadHtmlContent(activity, "http://rtr.innovid.com/r1.56579ddc7b3817.44076240;cb=[timestamp]", new DataCallback() {
//                    NetworkManager.loadHtmlContent(activity, "http://rtr.innovid.com/r1.559639b1c47664.80661473;cb=[timestamp]", new DataCallback() {
//                    NetworkManager.loadHtmlContent(activity, "http://rtr.innovid.com/r1.56401b146c4e06.14042993;cb=[timestamp]", new DataCallback() {
//                    NetworkManager.loadHtmlContent(activity, "http://cdn-tags.brainient.com/1716/fbf62fac-01df-42d4-98eb-c1cc4cdf91cd/html5/embed.js?proto=http", new DataCallback() {
//                    NetworkManager.loadHtmlContent(activity, "http://cdn-tags.brainient.com/1716/27389ef2-7581-4873-b0e4-6a695535664d/html5/embed.js?proto=http", new DataCallback() {
//                    NetworkManager.loadHtmlContent(activity, "http://rtr.innovid.com/r1.5595f4e8870d72.48026180;cb=[timestamp]", new DataCallback() {

                        String script;

                        @Override
                        public void onResponse(Response response) {
                            try {
                                script = response.body().string();
                                if (script.isEmpty()) {
                                    Utils.showError(activity, activity.getString(R.string.error_invalid_data));
                                } else {
                                    sCreativeData.mCreativeScript = script;
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            dataLoaded.onDataLoaded(Utils.createHtml(script));
                                        }
                                    });
                                }
                            } catch (IOException e) {
                                Utils.showError(activity, activity.getString(R.string.error_server_connection));
                                e.printStackTrace();
                            } finally {
                                response.body().close();
                            }
                        }
                    });
                } else {
                    Utils.showError(activity, activity.getString(R.string.error_invalid_data));
                }
            }
        };

        getCreativeData(activity, sClientData, callback);
    }

    private static CreativeData getEntry(Activity activity, Response response) {
        CreativeData creativeData = null;
        InputStream in = response.body().byteStream();

        try {
            creativeData = DataParser.parse(in);
        } catch (XmlPullParserException | IOException e) {
            Utils.showError(activity, e.getMessage());
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    Utils.showError(activity, e.getMessage());
                    e.printStackTrace();
                }
            }
            response.body().close();
        }
        return creativeData;
    }

    private static void getCreativeData(final Activity activity, ClientData clientData, final DataCallback dataCallback) {
        OkHttpClient client = new OkHttpClient();
        HttpUrl.Builder urlBuilder = HttpUrl.parse(BASE_URL).newBuilder();
        urlBuilder.addPathSegment(PATH);
        urlBuilder.addQueryParameter(PARAM_PUBLISHER_ID, clientData.mPublisherId);
        urlBuilder.addQueryParameter(PARAM_APP_NAME, clientData.mAppName);
        urlBuilder.addQueryParameter(PARAM_APP_BUNDLE_ID, clientData.mPackage);
        urlBuilder.addQueryParameter(PARAM_USER_ID, clientData.mIfa);
        if (clientData.mLatitude != null) {
            urlBuilder.addQueryParameter(PARAM_LATITUDE, clientData.mLatitude);
        }

        if (clientData.mLongitude != null) {
            urlBuilder.addQueryParameter(PARAM_LONGITUDE, clientData.mLongitude);
        }
        urlBuilder.addQueryParameter(PARAM_IP, clientData.mIp);
        urlBuilder.addQueryParameter(PARAM_UA, clientData.mUserAgent);
        Log.d(TAG, "server request url: " + urlBuilder.build().toString());

        final Request request = new Request.Builder()
                .url(urlBuilder.build().toString())
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Utils.showError(activity, e.getMessage());
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                dataCallback.onResponse(response);
            }
        });
    }

    static void sendToServer(final String url) {
        ServerRequestJob job = new ServerRequestJob(url);
        JobQueueManager.sJobManager.addJobInBackground(job);
    }

    private static void loadHtmlContent(final Activity activity, String url, final DataCallback callback) {
        OkHttpClient client = new OkHttpClient();
        Call call = client.newCall(new Request.Builder()
        .url(url)
        .build());
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Utils.showError(activity, e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                callback.onResponse(response);
            }
        });
    }

    static void requestServer(String url) throws Exception {
        if (url != null && URLUtil.isValidUrl(url)) {
            OkHttpClient client = new OkHttpClient();
            final Request request = new Request.Builder()
                    .url(url)
                    .build();

            Call call = client.newCall(request);
            Response response = call.execute();
            if (response.isSuccessful()) {
                response.body().close();
            } else {
                if (MraidSdk.sListener != null) {
                    MraidSdk.sListener.onError("Server connection error");
                }

                throw new Exception("Server connection error");
            }
        } else {
            if (MraidSdk.sListener != null) {
                Log.e(TAG, "Invalid tracker url");
            }
        }
    }
}
