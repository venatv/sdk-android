package tv.vena.mraidsdk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.FrameLayout;

class Controller {

    private CreativeData mCreativeData;
    private Bridge mBridge;
    private Context mContext;
    private AdWebView mAdWebView;
    private final FrameLayout mDefaultAdContainer;
    private final CloseableLayout mCloseableLayout;

    Controller(Context context) {
        this(context, new Bridge());
        mAdWebView = new AdWebView(mContext);
        mBridge.attachView(mAdWebView);

    }

    private Controller(Context context, Bridge bridge) {
        Bridge.BridgeListener bridgeListener = new Bridge.BridgeListener() {
            @Override
            public void onPageLoaded() {
                handlePageLoad();
            }

            @Override
            public void shouldOverrideUrlLoading(String url) {
                handleShouldOverrideUrl(url);
            }
        };

        mContext = context;
        mBridge = bridge;
        mBridge.setBridgeListener(bridgeListener);
        mDefaultAdContainer = new FrameLayout(mContext);
        mCloseableLayout = new CloseableLayout(mContext);
    }

    private void handlePageLoad() {
        mCreativeData = DataLab.getInstance().getCreativeData();
        mBridge.fireChangeEvent(ViewState.DEFAULT, true);
        mBridge.setScreenParams(0, 0, Dips.getWidth(mContext), Dips.getHeight(mContext));
        mBridge.notifyReady();

        if (mCreativeData != null) {
            for (String s : mCreativeData.mImpressions) {
                NetworkManager.sendToServer(s);
            }
        }
    }

    private void handleShouldOverrideUrl(String url) {
        mAdWebView.onPause();
        if (url.contains(Utils.MRAID_SCHEME)) {
            String command = Utils.getCommandFromUrl(url);
            if (command.equals(MraidJavascriptCommand.CLOSE.toJavascriptString())) {
                mBridge.nativeCallComplete(MraidJavascriptCommand.CLOSE);
            } else if (command.equals(MraidJavascriptCommand.OPEN.toJavascriptString())) {
                sendClickEvent();
                mBridge.nativeCallComplete(MraidJavascriptCommand.OPEN);
            } else {
                mBridge.nativeCallComplete(
                        MraidJavascriptCommand
                        .fromJavascriptString(command));
            }
        }

        String preparedUrl = Utils.prepareUrl(url);
        if (URLUtil.isValidUrl(preparedUrl) && !url.equals("about:blank")) {
            sendClickEvent();
            if (MraidSdk.sListener != null) {
                MraidSdk.sListener.onComplete();
            }
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.setData(Uri.parse(preparedUrl));

            mAdWebView.getContext().startActivity(i);
            ((Activity)mContext).finish();
        }
    }

    private void sendClickEvent() {
        if (mCreativeData != null) {
            for (String s : mCreativeData.mClicks) {
                NetworkManager.sendToServer(s);
            }
        }
    }

    void loadContent(@NonNull String htmlData) {
        mBridge.setContentHtml(htmlData);
        mDefaultAdContainer.addView(mAdWebView, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
        mDefaultAdContainer.setVisibility(View.VISIBLE);
    }

    FrameLayout getAdContainer() {
        mCloseableLayout.setBackgroundColor(Color.BLACK);
        mCloseableLayout.addView(mDefaultAdContainer);
        mCloseableLayout.setOnCloseListener(new CloseableLayout.OnCloseListener() {
            @Override
            public void onClose() {
                handleClose();
            }
        });
        return mCloseableLayout;
    }

    void handleClose() {
        if (MraidSdk.sListener != null) {
            MraidSdk.sListener.onComplete();
        }

        DataLab.getInstance().setCreative(null);
        if (mCreativeData != null) {
            NetworkManager.sendToServer(mCreativeData.mCloseEvent);
        }
        mBridge.fireChangeEvent(ViewState.HIDDEN, false);
        mBridge.nativeCallComplete(MraidJavascriptCommand.CLOSE);
        mAdWebView.onPause();
        mAdWebView.loadUrl("about:blank");
        mBridge.detach();
        ((Activity)mContext).finish();
    }

    String getUserAgent() {
        return mAdWebView.getSettings().getUserAgentString();
    }

    void pause() {
        mAdWebView.onPause();
    }

    void resume() {
        mAdWebView.onResume();
    }
 }
